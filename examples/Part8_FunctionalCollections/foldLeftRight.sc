////non-commutative
val l = List(1,2,3,4,5)

l.foldRight(1)(
      (i,a) => a + Math.pow(a,i).toInt)

l.foldLeft(1)(
      (a,i) => a + Math.pow(a,i).toInt)



