////map and filter version of company
//
case class Company( val name :String, val region :String, val avgSalary :Int )
case class Employee( val name :String, val companyName :String, val age :Int )

val companies = List( Company( "SAL", "HE", 2000 ),
  Company( "GOK", "DA", 2500 ),
  Company( "MIK", "DA", 3000 ) )

val employees = List( Employee( "Joana", "GOK", 20 ),
  Employee( "Mikey", "MIK", 31 ),
  Employee( "Susan", "MIK", 27 ),
  Employee( "Frank", "GOK", 28 ),
  Employee( "Ellen", "SAL", 29 ) )

//ArrayList<stuff> a = new Arraylist<>();
//for (emp e : emplyees){
//  if (e.age > 25){
//    int salaray = e.age * 100
//
//    for (com c : companies){
//      if (c.region == "DA" && c.name == e.companyName && c.avgSal < salaray){
//
//        a.add(new stuff(e,c,salary))
//      }
//    }
//  }
//}

val result =
  for { e <- employees
       if e.age > 25

       salary = e.age * 100

       c <- companies
       if c.region == "DA"
       if c.name == e.companyName
       if c.avgSalary < salary
  }
  yield ( e.name, c.name, salary - c.avgSalary )


val mapped =
  employees filter(x => x.age > 25) flatMap (
  x =>
      companies.filter(a => a.region == "DA")
        filter
        (y => x.companyName == y.name && y.avgSalary < x.age * 100)
        map
        (z => (x.name, z.name, x.age * 100 - z.avgSalary))
  )
println(s"Original: $result")
println(s"New     : $mapped")
