val list = 1 to 50 toList

list filter ( x => x % 2 == 0
  && x > 10 && x < 50)

val maps = Map("a" -> 1, "b" -> 2, "c" -> 3)

maps.filterKeys (_ == "a")

val (keys, values) = maps unzip

values.filter( _ == 1)

maps filter {
  case (k,v) => v == 1
}