
val list = List(1,2,3,4)

list map ((x: Int) => x % 2 == 0)

list map ( x => x % 2 == 0)

list foreach println
//mapping with maps
val prices = Map[String, Double]("tea"->1, "coffee"-> 2, "coke" ->2.5)
val prices_updated = prices.mapValues(x => x * 1.2)

//uses some pattern matching to achieve both
val bothMapped: Map[String, Double] =
  prices.map {
    case (k, v) => (k + "_new", v * 1.2)
  }

//folding left and right. See other example for
//non-commutative version
list.foldLeft(0)((a,i) =>
  {println(s"a is $a, i is $i");a + i}
)
list.foldLeft(0)(_ + _)
list.foldRight(0)((i, a) => a + i)

val str = "Hello World"


str.groupBy(_.toChar).map { c => (c._1, c._2.length) }

str.foldLeft(Map[Char,Int]() withDefaultValue 0) {
  (h,c) => h.updated(c, h(c)+1)
}




