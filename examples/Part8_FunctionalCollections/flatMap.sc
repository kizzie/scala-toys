def g(v:Int) = List(v-1, v, v+1)

val l = List(1,2,3,4)

l.map(x => g(x))

l.flatMap(x => g(x))