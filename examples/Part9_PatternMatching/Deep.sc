//case classes have the apply and unapply method defined
case class Location(city: String, country: String)
  case class Person(name: String, age: Int, loc: Location)

//create some people and locations
  val london = Location("London", "UK")
  val brussels = Location("Brussels", "Belgium")
  val nyc = Location("New York", "US")
  val p1 = new Person("fred", 45, london)
  val p2 = new Person("eric", 55, brussels)
  val p3 = new Person("jane", 12, nyc)

//pattern match based on the person
  def test(p: Person) =
    p match {
      case Person("fred", age, Location(city, country)) => s"fred lives in $city"
      case Person(name, _, Location(city, "Belgium")) => s"$name lives in Belgium"
      case Person(name, age, loc)
        if age > 18 && age < 65 => s"$name is of working age"
      case _ => "default"
    }
  test(p1) //called fred
  test(p2) // lives in belgium
  test(p3) // is under 18
  test(Person("alice", 25, Location("London","UK")))
