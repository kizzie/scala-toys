



import scala.util.{Failure, Success, Try}

def s (a: String): Try[Int] = {
  Try {
    a.toInt
  }
}

s("1")
s("hello")
val list = List("one", "2", "three", "4", "5")
val mapped = list map s

def output(l: List[Try[Int]]): Unit = {
  l match {
    case Nil => println("end of list list")
    case Success(a) :: tail => {println(s"Success: $a"); output(tail)}
    case Failure(ex : NumberFormatException) :: tail => {println(s"Error: ${ex.getMessage}"); output(tail)}
    case _ => println("?????")
  }
}

output(mapped)