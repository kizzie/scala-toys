val t = (1,2)
val s = (1,"bob", 45)

t match {
  case (a,b) => s"a is $a, b is $b"
}

s match {
  case (id, name, age) => s"id: $id, name: $name, age: $age"
  case (_, "bob", age) => s"I found bob, is he $age"
}