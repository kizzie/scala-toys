val m = 7;

m match {
  case 1 => println("it is one")
  case 2 => println("it is two")
  case _ => println("anything else")
}



// val list = List (1,2,3,4,5)

def sum(l: List[Int], s: Int): Int =
    l match {
        case Nil => s
        case h :: rest => {println(h);
          println(rest); sum(rest, s+h)}
      }

      val output = sum(List(1,2,3,4,5), 0)  // l  : Int = 15

