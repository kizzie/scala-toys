
class Foo (val name: String, val age: Int)

object Foo {
  def apply(name: String, age: Int) = new Foo(name, age)
  def unapply(f : Foo) = Some((f.name, f.age))
}

val f = Foo("Alice", 2)
//val f =
//Foo.unapply(f)
f match {
  case Foo("Alice", age) => println(s"hi alice $age")
  case Foo("Bob", _) => println("hi bob")
  case Foo(c, a) if a > 18 => println("over18")
  case Foo(c, a) => println(s"hi $c?")
  //case _ => println("Who are you?!")
}