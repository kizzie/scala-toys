//enumerations
object MyEnum extends Enumeration {
  type CollisionDetector = Value
  val JMONKEY, J3D, NONE, J3DSPHERE = Value
}

import MyEnum._

val cd : CollisionDetector = JMONKEY

if (cd == J3D) println("java 3d")
else if (cd == JMONKEY) println("Jmonkey")
else println("no clue")

