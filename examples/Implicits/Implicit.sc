implicit def intToString( x: Int) = s"$x"

val someInt = 15
someInt.concat("55")



implicit def someFunction() = println("I was a function")

trait foo {
  def bar(implicit x : () => Unit): Unit = {
    println("trait")
    x()
  }
}

class a
val b = new a with foo

b.bar
