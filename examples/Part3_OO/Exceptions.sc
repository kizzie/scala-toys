
def methodName = {
  throw new IllegalArgumentException("foo")
}

try {
  methodName
} catch {
  case ex : IllegalArgumentException => println("something went wrong")
  case ex : Exception => println("Could not even work out type")
}