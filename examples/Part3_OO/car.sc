//package classes
object Car {

  def apply(name: String, isSportsCar: Boolean) = {
    val c = new Car(name)
    c.isSportsCar = isSportsCar
    c
  }

  def apply(name: String) = new Car(name)

  private val numberOfWheels = 4
}

class Car private (val make: String) {
  private var speed = 0
  private var isSportsCar = false

  // Need to qualify access to a member in the object
  def howManyWheels = Car.numberOfWheels

  override def toString = s"This is my toString: " +
    s"make = $make, isSportsCar = $isSportsCar"
}
//val c = new Car("Ford")
//c.howManyWheels
//
val c2 = Car("Porsche", true)
val c2a = Car.apply("Porsche", true)

val list = Array(1,2,3,4,5)

val a = list(3)

list.apply(3)

//val c3 = Car.apply("BMW", true)
//
//
//val list = List(1,2,3,4)