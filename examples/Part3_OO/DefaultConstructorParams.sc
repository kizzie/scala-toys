class Person(val name: String = "Alice",
             val age: Int = 21)

val p = new Person(age = 22)

p.name

p.age