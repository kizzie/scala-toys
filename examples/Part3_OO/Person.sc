class Person {
  var age = 0;
}

val p = new Person

p.age;


class PersonWithConstructor
  (val name : String, val age : Int)

val p2 = new PersonWithConstructor("kat", 21)
p2.age
p2.name

class PersonWithOwnGetterAndSetter
(private val _name: String,
 private var _age: Int)
{
  def name = _name;

  def age = _age;
  def age_=(x : Int) = _age = x

}

val p3 = new PersonWithOwnGetterAndSetter("kat", 21)
p3.age
p3.age = 22
p3.age_=(23)
p3.age


class PersonWithExtraConstructor(val name: String, var age: Int){
  def this(name:String) = this(name, "0")
}


