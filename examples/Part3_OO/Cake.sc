class cake  (private var flavour : String) {

  def this() = this("default flavour")

  private var internal_slices = 8

  def slices = internal_slices
  def slices_=(x: Int) {
    internal_slices = internal_slices + x }

  override def toString = s"om nom nom $flavour"
}


val c = new cake("chocolate")
c.slices
c.slices = 9
c.slices

//c.flavour

class Person(val firstname: String,
             val surname: String,
             val age: Int) {
  println("creating a person")
  val a = 5
  def b = 6
}

val p = new Person("bob", "bunny", 2)

val _name = "kat"

println(s"this is ${_name}")


















