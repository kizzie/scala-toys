class Counter {
  private var count = 0;

  def increment = count += 1
  def counter = count;
}

val c = new Counter;
c.counter
c.increment
c.counter
