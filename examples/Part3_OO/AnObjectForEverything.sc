
case class firstname(firstname :String)

object firstname {
  def apply(fname: String) = {
     new firstname(fname)
  }
}
case class surname(surname:String)

case class Person(fname: firstname, sname:surname)

val p = Person(firstname("bob"),
              surname("Jones"))