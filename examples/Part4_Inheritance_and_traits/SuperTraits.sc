trait GrandParent {
  def doSomething = println("Grandparent")
}

trait ParentOne extends GrandParent {
  override def doSomething =
  {println("Parent1"); super.doSomething}
}
trait ParentTwo extends GrandParent {
  override def doSomething = {println("Parent2");
    super.doSomething}
}
trait midParent extends GrandParent {
  override def doSomething =
  {println("midParent"); super.doSomething}
}
trait ParentThree extends midParent {
  override def doSomething =
  {println("Parent3 goes to mid"); super.doSomething}
}
class a extends ParentOne
with ParentTwo
with ParentThree
new a().doSomething


trait Logger{
  def doSomething
}
trait ConcreteLogger {
  def doSomething = "hi"
}

val traitinit = new ConcreteLogger{}

abstract class foo extends Logger {
}

//object foo {
//  def apply() = {
//    new foo with
//  }
//}

val a = (a: Int, b:Int, c: Int) => a + b + c
val b = (a: Int) => a * 2
val c = a.curried
val kat1 = c(4)
val kat2 = kat1(5)
kat2(5)
//val d = (x: Int)(y: Int) => {x * y}
val both =  c compose b