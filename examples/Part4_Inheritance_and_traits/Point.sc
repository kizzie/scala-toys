class Point(xc: Int, yc: Int) {
  val x: Int = xc
  val y: Int = yc

  def move(dx: Int, dy: Int): Point =
    new Point(x + dx, y + dy)
}


class WeightedPoint(xc: Int, yc: Int, wt: Int)
  extends Point(xc, yc) {
  val weight = wt

  def compareWith(pt: WeightedPoint): Boolean =
    (pt.x == x) && (pt.y == y) && (pt.weight == weight)

  override def move(dx: Int, dy: Int): WeightedPoint =
    new WeightedPoint(x + dy, y + dy, weight)

  override def toString = s"x: $x, y: $y, weight: $weight"
}


val pt = new WeightedPoint(1,2,3)
pt.x
pt.y
pt.weight
pt.move(2,3)