//abstract trait - does not contain all implementation
trait Logger {
  def Log(msg:String)
}

class foo extends Logger {
  def Log(msg:String) = println(msg)
}

//concrete logger does have some implementation
trait ConcreteLogger {
  def Log(msg:String) = {}
}


class bar

val b2 = new bar with ConcreteLogger

val b = new bar with Logger {
  def Log(msg:String) = println(msg)
}

//create instances of the trait using anon object
//creates a new object and applies the trait
val cl = new ConcreteLogger{}

//creates a new object, adds the method, applies the trait
val f = new Logger {
  def Log(msg:String) = println(msg)
}