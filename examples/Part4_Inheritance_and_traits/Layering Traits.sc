trait base {
  def doSomething = {}
}

trait one extends base {
  override def doSomething = println("one")
}

trait two extends base {
  override def doSomething = println("two")
}

trait three extends base {
  override def doSomething = println("three")
}

class foo

val f = new foo with one with two with three
f.doSomething

trait unrelated {
  def doSomething = println("hi there")
}

//won't work by default, inrelated doSomething traits
val g = new foo with one with two with three
  with unrelated {

//  def doSomethingUnrelated =
//    super[unrelated].doSomething
//
  override def doSomething =
    super[unrelated].doSomething
}
g.doSomething
//g.doSomethingUnrelated