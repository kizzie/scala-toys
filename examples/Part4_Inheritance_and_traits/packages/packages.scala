
object packages extends App {

  import foo.Person

  val p = new Person("Alice", 21)
  println(p.d.name)
}

object anotherObject {
//import not valid here
//  val p = new Person("Bob", 22)
}