abstract class abs {
  def absMethod: Double
  val variable : Int
}

class concrete extends abs {
  def absMethod: Double = {println(variable); variable}
  val variable = 5;
}

val f = new concrete
f.absMethod
f.variable


//types

abstract class abs2 {
  type T
  def absMethod: T
  val variable : T
}

class concrete2 extends abs2 {
  type T = Double

  def absMethod: T = {
    println(variable);
    variable
  }
  val variable: T = 5;
}

val f2 = new concrete2
f2.absMethod
f2.variable


