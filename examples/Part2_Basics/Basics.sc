
def a = println("hello")
println("hello")
//inferring type
val n = 5;
var m = 10;

//n = 10; //not going to work
m = 15;

//declaring type
val d : Double = 2


//equivilence
2.+(2)
2+2

//statement blocks use {}
val output = {val a: Double = 10; val b = 20; a/b}

//if statement
val something = true
if (something)
  println("it was true")
else
  println("it was false")

//loops
for (x <- "Hello world") println(x)
for (x <- 1 to 10) println(x)
for (x <- 1 until 10) println(x)
for (x <- 1 to 10) yield x % 3
for (x <- 1 to 10 if x % 2 == 0) yield x
//functions
def sum(a: Int, b:Int) = a + b
sum(1,2)
def sum2(a: Int = 10, b: Int = 10) = a + b
sum2()
sum2(1)
sum2(1,2)
sum2(b = 2)
//variadic function (see: http://en.wikipedia.org/wiki/Variadic_function)
def sum(n : Double*) = {
  var result = 0.0
  for (i <- n)
    result += i
  result   // evaluate to get return value
}
val s = sum(1.1, 2.2, 3.3)  // 6.6
//return types for functions
def sumWithReturnType(a: Int, b: Int): Double =
  a + b
//string processing
val name = "kat"
println(s"Hello $name")
println("a \n b")
println(raw"a \n b")






