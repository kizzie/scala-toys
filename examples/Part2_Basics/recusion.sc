
//def aMethod(): returntype = {
//
//}

5 * 4 * 3 * 2 * 1

def fact(x : Int, sum : Int = 1): BigInt = {
  if (x == 1) sum
  else fact(x - 1, sum * x)
}


fact(5)


def factNotTail(x: Int): BigInt  = {
  if (x == 1) 1
  else x * factNotTail(x - 1)
}

factNotTail(600)
fact(1000000000)


val list = 5 to 1 by -1
var result = 1

for (c <- list)
  result = c * result

result