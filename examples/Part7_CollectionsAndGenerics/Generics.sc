def method[A](x: A) = s"The value passed in was $x"


method(1)
method("hello")



class something[A, B](val first : A, val second : B) {
  def returnA : A = first
  def returnB : B = second
  def returnString: String = s"type of A is ${first.getClass}, type of B is ${second.getClass}"
}
val s = new something(1, "hello")
s.returnA
s.returnB
s.returnString