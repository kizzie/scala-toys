
//tuples
val t = (1, "kat", 'k')

t._1
t._2
t._3

val (id, name, _) = t
id
name

//lists
val list = List(1,2,3,4,5)

//find nil at the end of the list
//using recursion
def func(list : List[Int]): Unit = {
  if (list == Nil) println("end of list")
  else {
    println(s"head of the list is ${list.head}")
    func(list.tail)
  }
}
func(list)
//maps
val map = Map("Dave" -> 45, "Paul" -> 41, "Bill" -> 55)
map.get("Dave")
map.getOrElse("Kat", 0)

val map2 = Map("Angus" -> "A good rabbit",
  "Shadow" -> "Mischief",
  "Honey" -> "Biiiiiiig dog")
map2.apply("Angus")
map2.applyOrElse("Shadow", (x: String) => s"Could not find key $x" )
map2.applyOrElse("Peter", (x: String) => s"Could not find key $x" )


//zipping
val drinkshop = Map[String, Double]("Coke" -> 1, "Tea" -> 1.5, "Coffee" -> 2)

val (one, two) = drinkshop.unzip


for (c <- one) println(c)
val three = for (c <- two) yield c * 1.5

val drinkshopUpdated = one zip three

