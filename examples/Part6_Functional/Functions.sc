//a normal function

def sum(a: Int, b:Int) = a + b

//anon function

val sumAnon = (a: Int, b: Int) => a+b

sumAnon(1,2)

//we can pass anon functions around as parameters

def doFunc(x: Int, f: Int=>Int) = f(x)

doFunc(2, x => x + 1)

def doFunc(x: Int, y:Int, f: (Int, Int)=>Int) = f(x, y)

doFunc(1,2, sumAnon)
doFunc(1,2, sum _) // <- converts the def into an anon function

