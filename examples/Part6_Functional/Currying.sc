val sum = (a: Int, b: Int, c: Int) => a + b + c

val a = sum.curried
a(1)(2)(3)

val b = a(1)
val c = b(2)
val d = c(3)

