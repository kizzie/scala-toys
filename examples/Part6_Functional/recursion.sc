
import scala.annotation._

//@tailrec
def fact(n: Int) : Int =
{
  if (n == 0) 1
  else fact(n -1) * n
}

def fact3(n: Int) = {
  def fact2(n: Int, acc: Int): Int = {
    if (n == 0) acc
    else fact2(n - 1, n * acc)
  }

  fact2(n, 1)

}

fact3(5)



@tailrec
def fact4(n: Int, acc: Int = 1): Int = {
  if (n == 0) acc
  else fact4(n - 1, n * acc)
}

fact4(5)