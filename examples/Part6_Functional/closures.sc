def counter() = {
  //this is the closure
  var c = 0

  //this is returned
  () => {c += 1; c}
}

var count = counter()

for (i<- 1 to 10) println(count())

count = null

count = counter()
count()
count()


def fib() = {
  //this is the closure
  var t = (1,-1)

  //this is returned
  () => {
    t = (t._1 + t._2, t._1)
    t._1
  }
}

val f = fib()
for (i <- 1 to 10) println (f())