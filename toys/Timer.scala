// from http://www.ibm.com/developerworks/library/j-scala01228/
object Timer
{
  def goLoop(Second : Int, callback: () => Unit) 
  {
    while (true)
    {
      callback()
      Thread.sleep(Second * 1000)
    }
  }

  def main(args: Array[String]) = goLoop(1, () => Console.println("Time flies... oh, you get the idea."))
  
}