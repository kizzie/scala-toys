/**
 * Created by kat on 01/06/15.
 */

/** Week one questions based on coursera course **/
object Week1 {

  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row) print(pascal(col, row) + "  ")
      println()
    }

    println(balance("(if (zero? x) max (/ 1 x))".toList))
    println(balance("I told him (that it�s not (yet) done). (But he wasn�t listening)".toList))
    println(balance(":-)".toList));
    println(balance("(()".toList));

    println(countChange(10, List(10, 5, 2, 1)));
  }

  /**
   * calculating pascal's triangle
   */
  def pascal(c: Int, r: Int): Int = {

    r match {
      case 0 => 1;
      case _ =>
        c match {
          case 0 => 1;
          case _ if (c / r == 1) => 1
          case _ if (c / r != 1) => pascal(c - 1, r - 1) + pascal(c, r - 1)
        }
    }
  }

  /**
   * Balancing Parameters in a string
   */
  def balance(chars: List[Char]): Boolean = {
    def balance2(chars: List[Char], number: Int): Boolean = {
      chars match {
        case head :: tail if (head == '(') => balance2(tail, number + 1);
        case head :: tail if (head == ')') => balance2(tail, number - 1)
        case _ :: tail => balance2(tail, number)
        case Nil => if (number == 0) true else false
      }
    }

    balance2(chars, 0);

  }

  //counting change
  //  def count( n, m ):
  //    if n == 0:
  //        return 1
  //    if n < 0:
  //        return 0
  //    if m <= 0 and n >= 1: #m < 0 for zero indexed programming languages
  //        return 0
  //
  //    return count( n, m - 1 ) + count( n - S[m], m )

  def countChange(money: Int, coins: List[Int]): Int = {
    if (money == 0) 1; //if we ran out of money it worked, so return
    else if (money < 0) 0; // if money is less than 0 then this isn't a valid solution, return no
    else if (coins.length <= 0 && money >= 1) 0; //if there are no more coins to try and we still have money, also return no
    else countChange(money, coins.tail) + countChange(money - coins.head, coins); //otherwise continue counting
  }

}



