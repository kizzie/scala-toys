

object main2 {
  def main(args: Array[String]) {

    //build the graph
    val map = buildMap

    //now we want to do graph search to find the routes between start and and h
    search("h", map, List("a"))
  }

  def buildMap: graph = {
    //graph nodes and their connections
    val a = new graph("a")
    val b = new graph("b")
    val c = new graph("c")
    val d = new graph("d")
    val e = new graph("e")
    val f = new graph("f")
    val g = new graph("g")
    val h = new graph("h")
    val i = new graph("i")

    a.addConnection(b)
    a.addConnection(d)
    a.addConnection(f)
    b.addConnection(d)
    b.addConnection(e)
    b.addConnection(c)
    c.addConnection(i)
    d.addConnection(f)
    e.addConnection(i)
    f.addConnection(g)
    g.addConnection(e)
    i.addConnection(h)

    //return statement
    a
  }

  
  //assumptions, the start node is the current graph node.
  def search(end: String, map: graph, list : List[String]) {

    //get the list of connections
    var conn = map.connections;

    //if it is empty, say done
    if (conn.length == 0) println("end of list");

    else {
      for (c <- conn) {
        if (c.name != end) search(end, c, c.name :: list);
        else println(c.name :: list);
      }
    }
  }

}

class graph(namec: String) {

  //variables, name and a list of connections
  var name: String = namec
  var connections: List[graph] = List()

  //add a single connection
  def addConnection(conn: graph) { connections ::= conn }


  //find an individual connection calling rule
  def findConnection(toFind: String): graph = {
    findConnection(toFind, connections)
  }

  //find an individual connection, recursive steps.
  private def findConnection(toFind: String, currentMap: List[graph]): graph = {
    currentMap match {
      case head :: tail if head.name != toFind => findConnection(toFind, tail)
      case head :: tail if head.name == toFind => head
      case Nil => null;
    }
  }

  //toString method
  override def toString: String = s"$name: (${buildConnString(connections)})"

    //builds the connection list from the list using recursion
  private def buildConnString(list: List[graph]): String = {
    list match {
      case head :: tail => s"${head.name}, " + buildConnString(tail)
      case Nil => ""
    }
  }
}