object stuff {
println("hello")                                  //> hello

def factorial (n : Int)  = {
	def fact(n : Int, acc: Int): Int = {
		if (n == 0) acc else fact(n-1, acc * n)
	}
	
	fact(n, 1)
}                                                 //> factorial: (n: Int)Int

factorial(2)                                      //> res0: Int = 2
factorial(3)                                      //> res1: Int = 6
factorial(4)                                      //> res2: Int = 24

}