

object main {
  def main(args: Array[String]){
    var ceaser = new ceaser(7);
    
    println(ceaser.encrypt("hello world"))
    println(ceaser.decrypt(ceaser.encrypt("hello world")))
    
  }
}

class ceaser(val shift : Int) {


  def encrypt(word : String) : String = word map encryptC
  
  def decrypt(word : String) : String = word map decryptC
  
  //get get the character -
  // a to give the number in the alphabet,
  // add the shift,
  // then add it back to a, finally convert to char
  def encryptC(c: Char) : Char = ('a'+((c-'a'+shift)%26)).toChar
  
  def decryptC(c : Char) : Char = ('a'+((c+'a'+shift)%26)).toChar
  
}