object mainList {
  def main(args: Array[String]) {

    //create an array
    var array: List[String] = List("Apple", "Banana", "Orange", "Cookie", "Pear");

    printArrayForward(array);
    println("---")
    printArrayBackwards(array)
  }
  

  def printArrayForward(arr: List[String]) {
    arr match {
      case Nil => println("Empty List") // alternative: when there is nothing left.
      //case hd => printArrayForward(hd) //end point, no tail. Can't have both
      case hd :: tail => println(hd); printArrayForward(tail)
    }

    //java method
    //    if (arr.length > 0) {
    //      val head = arr.head
    //      val tail = arr.tail
    //
    //      println(head)
    //
    //      printArray(tail)
    //    }
  }
  
  def printArrayBackwards(arr: List[String]) {
    arr match {
      case Nil => println("Empty, going back up the stack");
      case hd :: tail => printArrayBackwards(tail); println(hd);
    }
  }
}