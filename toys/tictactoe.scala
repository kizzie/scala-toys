object tictactoe {
  def main(args: Array[String]) {
    playGame
  }

  def playGame {
    val board = new GameBoard;

    //main game loop
    var player = 1;
    while (board.hasWinner == "") {
      //if there are spaces to go!
      //draw board
      board.printBoard;

      println(s"Player $player, please choose your move");

      var validMove = false;

      while (!validMove) {
        //while no valid input
        //ask for input
        println("Level (0-2)");
        var level = readInt;
        println("Square (0-8)");
        var square = readInt;
        //try to update board
        validMove = board.updateBoard(square, level, player)

        if (!validMove) {
          println("Move not valid, try again");
        }
      }

      //switch player
      if (player == 1) player = 2
      else player = 1;
    }

    board.printBoard
    println(board.hasWinner)
  }
}

class GameBoard {

  //var board: Array[Array[Int]] =
  var board =
    Array(
      Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
      Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
      Array(0, 0, 0, 0, 0, 0, 0, 0, 0))

  def hasWinner: String = {

    //need to check all the combinations. 

    /* One level
     * 0,1,2
     * 3,4,5
     * 6,7,8
     * 
     * 0,3,6
     * 1,4,7
     * 2,5,8
     * 
     * 0,4,8
     * 3,4,6
     * 
     * Three levels - same numbers, but must be on three levels consecutively
     * or all three the same number across the three levels
     * 
     */

    var result: Int = -1;

    for (i <- 0 to 2) {
      var l: Array[Int] = board(i);
      l match {
        //horizontal
        case Array(a, b, c, _, _, _, _, _, _) if (a == b && b == c && a != 0) => result = a;
        case Array(_, _, _, a, b, c, _, _, _) if (a == b && b == c && a != 0) => result = a;
        case Array(_, _, _, _, _, _, a, b, c) if (a == b && b == c && a != 0) => result = a;
        //vertical
        case Array(a, _, _, b, _, _, c, _, _) if (a == b && b == c && a != 0) => result = a;
        case Array(_, a, _, _, b, _, _, c, _) if (a == b && b == c && a != 0) => result = a;
        case Array(_, _, a, _, _, b, _, _, c) if (a == b && b == c && a != 0) => result = a;
        //diagonal
        case Array(a, _, _, _, b, _, _, _, c) if (a == b && b == c && a != 0) => result = a;
        case Array(_, _, a, _, b, _, c, _, _) if (a == b && b == c && a != 0) => result = a;
        case _ => ; //default case
      }
    }

    //if it has not been found in any of the 1d parts, start looking at 3d lines
    //Must be a better way to do this, 3D array? 
    if (result == -1) {
      board match {
        //same square on all three types
        case Array(
        Array(a, _, _, _, _, _, _, _, _),
        Array(b, _, _, _, _, _, _, _, _),
        Array(c, _, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, a, _, _, _, _, _, _, _),
        Array(_, b, _, _, _, _, _, _, _),
        Array(_, c, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, a, _, _, _, _, _, _),
        Array(_, _, b, _, _, _, _, _, _),
        Array(_, _, c, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, a, _, _, _, _, _),
        Array(_, _, _, b, _, _, _, _, _),
        Array(_, _, _, c, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, _, a, _, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, _, c, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, _, _, a, _, _, _),
        Array(_, _, _, _, _, b, _, _, _),
        Array(_, _, _, _, _, c, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, _, _, _, a, _, _),
        Array(_, _, _, _, _, _, b, _, _),
        Array(_, _, _, _, _, _, c, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, _, _, _, _, a, _),
        Array(_, _, _, _, _, _, _, b, _),
        Array(_, _, _, _, _, _, _, c, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(_, _, _, _, _, _, _, _, a),
        Array(_, _, _, _, _, _, _, _, b),
        Array(_, _, _, _, _, _, _, _, c)) if (a == b && b == c && a != 0) => result = a;
        //0
        case Array(
        Array(a, _, _, _, _, _, _, _, _),
        Array(_, b, _, _, _, _, _, _, _),
        Array(_, _, c, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;
        case Array(
        Array(a, _, _, _, _, _, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, _, _, _, _, _, c)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(a, _, _, _, _, _, _, _, _),
        Array(_, _, _, b, _, _, _, _, _),
        Array(_, _, _, _, _, _, c, _, _)) if (a == b && b == c && a != 0) => result = a;

        //1
        case Array(
        Array(_, a, _, _, _, _, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, _, _, _, _, c, _)) if (a == b && b == c && a != 0) => result = a;

        //2
        case Array(
        Array(_, _, a, _, _, _, _, _, _),
        Array(_, b, _, _, _, _, _, _, _),
        Array(c, _, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, a, _, _, _, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, _, _, _, c, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, a, _, _, _, _, _, _),
        Array(_, _, _, _, _, b, _, _, _),
        Array(_, _, _, _, _, _, _, _, c)) if (a == b && b == c && a != 0) => result = a;

        //3
        case Array(
        Array(_, _, _, a, _, _, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, _, _, c, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        //4

        //5
        case Array(
        Array(_, _, _, _, _, a, _, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, _, c, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        //6
        case Array(
        Array(_, _, _, _, _, _, a, _, _),
        Array(_, _, _, b, _, _, _, _, _),
        Array(c, _, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, _, _, _, _, a, _, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, _, c, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, _, _, _, _, a, _, _),
        Array(_, _, _, _, _, _, _, b, _),
        Array(_, _, _, _, _, _, _, _, c)) if (a == b && b == c && a != 0) => result = a;

        //7
        case Array(
        Array(_, _, _, _, _, _, _, a, _),
        Array(_, _, _, _, b, _, _, _, _),
        Array(_, c, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        //8
        case Array(
        Array(_, _, _, _, _, _, _, _, a),
        Array(_, _, _, _, _, _, _, b, _),
        Array(_, _, _, _, _, _, c, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, _, _, _, _, _, _, a),
        Array(_, _, _, _, b, _, _, _, _),
        Array(c, _, _, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        case Array(
        Array(_, _, _, _, _, _, _, _, a),
        Array(_, _, _, _, _, b, _, _, _),
        Array(_, _, c, _, _, _, _, _, _)) if (a == b && b == c && a != 0) => result = a;

        //default, no winner
        case _ => "";
      }
    }

    if (result > -1) {
      s"Player $result won"
    } else {
      "";
    }
  }

  //don't use lists - immutable is awkward
  //  def updateBoard(square: Int, dim: Int, player: Int): Boolean = {
  //
  //    //need to check the square isn't occupied beforehand
  //    if (board(dim)(square) == 0) {
  //      //change the value of the right element
  //      var newLevel = board(dim).updated(square, player);
  //
  //      //put the new level back in the board
  //      board = board.updated(dim, newLevel)
  //      true;
  //    } else {
  //      false;
  //    }
  //  }

  def updateBoard(square: Int, dim: Int, player: Int): Boolean = {
//    println(s"dim is $dim and sqaure is $square")
    if (square < 9 && dim < 3) {
      //need to check the square isn't occupied beforehand
//      println(board(dim)(square))
      if (board(dim)(square) == 0) {
        //change the value of the right element
        board(dim)(square) = player;
        true; //return value
      } else {
        false; //return value
      }
    } else {
      false;
    }
  }

  def printBoard {
    //offset for each line
    var offset = 0;
    //for each board
    for (j: Int <- 0 to 2) { //could have used for ( j <- board) or similar to get the 1D array methods?
    //for each element in the baord
    //work out the tab offset
    var tab = " " * offset;
      for (i: Int <- 0 to 8) {
        //if we're at the end of a batch of three, start a new line and add the tabs
        if (i % 3 == 0) { print("\n" + tab); print(s"$j:"); }
        //then print the element followed by a comma
        board(j)(i) match {
          case 0 => print(" , ");
          case 1 => print("o, ");
          case 2 => print("x, ");
        }
      }
      //printed out all of a board, so add more to the offset
      offset += 7;
    }
    println();
  }

}

